<#if articlelist ? exists>
    <#list articlelist as article>
        <#if article_index lt 10>
        <div class="layui-row layui-col-space15">
            <li class="layui-col-md3">
                <a class="fly-case-img" href="${channel.channel_catalog+article.article_pk+".html"}" target="_blank">
                    <img src="${article.article_titleimg}" alt="${article.article_title}">
                </a>
            </li>
            <li class="layui-col-md6 layui-col-md-offset1">
                <h2><a href="${channel.channel_catalog+article.article_pk+".html"}" target="_blank">${article.article_title}</a></h2>
                <p class="fly-case-desc">${article.article_content}</p>
            </li>
        </div>
        </#if>
    </#list>
</#if>