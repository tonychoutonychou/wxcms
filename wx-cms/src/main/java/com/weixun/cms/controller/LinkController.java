package com.weixun.cms.controller;


import com.jfinal.json.JFinalJson;
import com.jfinal.plugin.activerecord.Page;
import com.jfinal.plugin.activerecord.Record;
import com.weixun.cms.service.ChannelArticleService;
import com.weixun.cms.service.ChannelService;
import com.weixun.cms.service.LinkService;
import com.weixun.cms.service.SiteService;
import com.weixun.comm.controller.BaseController;
import com.weixun.model.CmsLink;
import com.weixun.utils.ajax.AjaxMsg;
import com.weixun.utils.freemarker.FreemarkerUtils;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class LinkController extends BaseController {

    LinkService linkService = new LinkService();
    SiteService siteService = new SiteService();

    private Record  getsite()
    {
        List<Record>  records =siteService.findList("");
        return records.get(0);
    }

    /**
     * 基于layui的分页
     */
    public void pages(){
        int pageNumber = getParaToInt("page");
        int pageSize = getParaToInt("limit");
        String link_title = getPara("link_title");
        Page<Record> list = linkService.paginate(pageNumber,pageSize,link_title);//获得用户信息
        renderPageForLayUI(list);
    }


    /**
     * 查询数据列表
     */
    public void list()
    {
        String link_pk = getPara("link_pk");
        List<Record> records = linkService.findList(link_pk);
        renderJson(JFinalJson.getJson().toJson(records));
    }


    /**
     * 删除数据
     */
    public void delete()
    {
        AjaxMsg ajaxMsg = new AjaxMsg();
        String link_pk = this.getPara("link_pk");
        int res =linkService.deleteById(link_pk);
        if (res >0) {
            ajaxMsg.setState("success");
            ajaxMsg.setMsg("删除成功");
        }
        else
        {
            ajaxMsg.setState("fail");
            ajaxMsg.setMsg("删除失败");
        }

        renderJson(ajaxMsg);
    }

    /**
     * 保存方法
     */
    public void saveOrUpdate()
    {
        AjaxMsg ajaxMsg = new AjaxMsg();
        boolean res = false;
        try {

            CmsLink link = getModel(CmsLink.class,"");
            if (link.getLinkPk() != null && !link.getLinkPk().equals(""))
            {
                //更新方法
                res = link.update();
            }
            else {
                //保存方法
                res = link.save();
            }
            if(res)
            {
                createListPage();
                ajaxMsg.setState("success");
                ajaxMsg.setMsg("保存成功");
            }
            else
            {
                ajaxMsg.setState("fail");
                ajaxMsg.setMsg("保存失败");
            }
        }catch (Exception e)
        {
            e.getMessage();
            ajaxMsg.setState("fail");
            ajaxMsg.setMsg("保存失败");
        }
        renderJson(ajaxMsg);
    }

    public void createListPage()
    {
        List<Record> records = linkService.findList("");
        Map<String, Object> data = new HashMap<>();
        data.put("linklist", records);
        /**
         * 模板路径
         * /template/qhrst
         */
        String templatePath = getRequest().getSession().getServletContext().getRealPath(getsite().getStr("site_template"));
        /**
         * 索引页模板文件
         */
        String templateName = "link.ftl";
        /**
         * 生成html页面路径
         */
        //String targetHtmlPath = getpath().getStr("site_static") + tmpchannel.getChannel_pages();
        String targetHtmlPath = getsite().getStr("site_static")+"pages/comm/link.html";

        /**
         *调用生成模板的方法
         */
        FreemarkerUtils.crateHTML(data, templatePath, templateName, targetHtmlPath);
    }

}
